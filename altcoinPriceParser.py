#!/usr/bin/python3

# http://docs.python-guide.org/en/latest/scenarios/scrape/

import urllib.request, json, sys, datetime

SHOWINCURRENCY = "USD"
CRYPTOCURRENCY = "BTC,ETH,DASH,LTC,XMR,XRP,ADA,XLM,ZCL,NEO,BTCP"
allCryptos = 0

def printResult(url, cryptoCurrency, showInCurrency):
	jsonReturned = urllib.request.urlopen(url)
	data = json.load(jsonReturned)
	print("----------------------------------------"+str( datetime.datetime.now() )+"-----------------------------")
	for crypto in data:
		print(crypto+" price in "+showInCurrency+" is: "+str("{:,}".format(data[crypto][showInCurrency])))

def makeUrl(cryptoCurrency, showInCurrency):
	return 'https://min-api.cryptocompare.com/data/pricemulti?fsyms='+cryptoCurrency+'&tsyms='+showInCurrency

def chooseFiatCurrency():
  global SHOWINCURRENCY
  showinchoice = input('Which currency do you want to see in? g for GBP, e for EURO, d for USD dollars (USD is default)> ').lower()
  if showinchoice == 'g':
    SHOWINCURRENCY = "GBP"
  elif showinchoice == 'e':
    SHOWINCURRENCY = "EUR"
  else:
    pass # Use the default which is set at top of script
  print("*-*-* You have chosen fiat currency of "+str(SHOWINCURRENCY)+" *-*-*")

def chooseCryptoCurrency():
  global CRYPTOCURRENCY
  cryptochoice = input('Which currency do you want to check? b for bitcoin BTC, e for ethereum ETH, d for dash, m for monero XMR, l for Litecoin LTC, r for ripple XRP, a for Cardano ADA, s for Stellar Lumens XLM, z for ZClassic ZCL, n for NEO, btcp for BTCP, or just ENTER for all (all is default)> ').lower()
  if cryptochoice == 'b':
    CRYPTOCURRENCY = "BTC"
  elif cryptochoice == 'e':
    CRYPTOCURRENCY = "ETH"
  elif cryptochoice == 'd':
    CRYPTOCURRENCY = "DASH"
  elif cryptochoice == 'l':
    CRYPTOCURRENCY = "LTC"
  elif cryptochoice == 'm':
    CRYPTOCURRENCY = "XMR"
  elif cryptochoice == 'a':
    CRYPTOCURRENCY = "ADA"
  elif cryptochoice == 'r':
    CRYPTOCURRENCY = "XRP"
  elif cryptochoice == 's':
    CRYPTOCURRENCY = "XLM"
  elif cryptochoice == 'z':
    CRYPTOCURRENCY = "ZCL"
  elif cryptochoice == 'n':
    CRYPTOCURRENCY = "NEO"
  elif cryptochoice == 'btcp':
    CRYPTOCURRENCY = "BTCP"
  else:
   pass # Use the default, which is set at the top of the script

try:
  choices = sys.argv[1]
  if choices.lower() == 'c':
    chooseFiatCurrency()
    chooseCryptoCurrency()
  else:
     raise ValueError('Arg was not a c')
except:
  print("==================================NB:=========================================================")
  print("Starting this script with the argument 'c' will give you more choices... E.g. altcoinPriceParser.py c")
  print("==============================================================================================")

printResult( 
	makeUrl(CRYPTOCURRENCY, SHOWINCURRENCY), 
	CRYPTOCURRENCY, 
	SHOWINCURRENCY
)
