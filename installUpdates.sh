#!/bin/sh

# fetch list of available updates
sudo apt update;

# install updates for the current packages you have
sudo apt -y upgrade;

# install newer versions and new packages
sudo apt -y dist-upgrade

# install security updates
sudo unattended-upgrade

# install packages
sudo apt install

# free up space
sudo apt-get -y autoclean;
sudo apt-get -y autoremove;
