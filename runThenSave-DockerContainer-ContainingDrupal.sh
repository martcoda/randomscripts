RUNNING:
docker run -p 52080:80 -p 3306:3306 -it ubuntu:lemp-installed
...this will take you into a terminal in the running image as root
START SERVICES
in root home dir there is script ./startServices

ACCESS WEBPAGES FROM HOST OS
then back on host OS navigate to 127.0.1.1:52080 in firefox and play around. 

EXIT CONTAINER
then afterward use exit to exit it

SAVING CHANGES
then use docker ps -l to see the image checksum of the image you just exited

then use the following to save changes to it: 

docker commit <imagechecksum> <imagename>
e.g. docker commit a7d78b8a87df8 ubuntu:lemp-installed
