#!/usr/bin/python3

#http://docs.python-guide.org/en/latest/scenarios/scrape/
import requests
from lxml import etree

#url = 'http://open.live.bbc.co.uk/weather/feeds/en/2650225/3dayforecast.rss'
url = 'https://weather-broker-cdn.api.bbci.co.uk/en/forecast/rss/3day/2650225'
page = requests.get(url)
tree = etree.fromstring(page.content)

#titles = tree.xpath('//')
items = tree.xpath('//title/text()')


#print(repr(items))

try:
  print(items[0])
except:
  print('got an exception for item 0')
'''try:
  print(items[1])
except:
  print('got exception for item 1')
'''
try:
  print(items[2])
except:
  print('got an exception for item 2')
try:
  print(items[3])
except:
  print('got an exception for item 3')
try:
  print(items[4])
except:
  print('got an exception for item 4')
