// NB: compile using- g++ -std=c++11 -pthread testMutex.cpp -o testMutex.exe
#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <stdio.h>
#include <chrono>
#include <list>

using namespace std;

mutex mtx;
list<int> q; //from codeyarns.com/2015/01/14/how-to-use-lock_guard-in-c/
int currentThread = 0;

void pushToQueue(int threadId){
		lock_guard<mutex>guard(mtx); // Locks mutex for the remainder of this function ... 
		q.push_front(threadId);
}

int main(){
	cout << "Beginning" << endl;
	int numThreads = 10;
	thread threads[numThreads];

	for(int c=0; c < numThreads; c++){
		threads[c]  = thread(pushToQueue,c);
	}

	for(auto& th : threads) th.join(); // From cplusplus.com/reference/mutex/lock_guard/

	for(auto& entry : q) cout << "Queue has " << entry << endl;

	cout << "...finito..." << endl;

	return 0;
}
